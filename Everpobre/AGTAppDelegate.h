//
//  AGTAppDelegate.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 11/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTCoreDataStack;

@interface AGTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AGTCoreDataStack *model;


@end
