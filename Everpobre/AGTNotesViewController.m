//
//  AGTNotesViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 14/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTNotesViewController.h"
#import "AGTNote.h"
#import "AGTPhoto.h"
#import "AGTNotebook.h"
#import "AGTNoteViewController.h"

@interface AGTNotesViewController ()
@property (strong, nonatomic) AGTNotebook *notebook;
@end

@implementation AGTNotesViewController

-(id) initWithNotebook:(AGTNotebook *) notebook
fetchedResultsController:(NSFetchedResultsController *) controller
                 style:(UITableViewStyle) style{
    
    
    if (self = [super initWithFetchedResultsController:controller
                                                 style:style]) {
        _notebook = notebook;
        self.title = notebook.name;
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                            target:self
                            action:@selector(addNote:)];
    
    self.navigationItem.rightBarButtonItem = btn;
    
}

-(void)addNote:(id) sender{
    
    AGTNote *n =[AGTNote noteWithName:@"Naomi VonKreeps"
                             notebook:self.notebook
                            inContext:self.notebook.managedObjectContext];
    n.photo = [AGTPhoto photoForNote:n
                           inContext:n.managedObjectContext];
    
    n.photo.image = [UIImage imageNamed:@"naomi.jpg"];
    
    n.text = @"Administradora de bases de datos, fan de StarWars y seguidora de Ministry.";
    
}

#pragma mark - Data Source
-(UITableViewCell *) tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // la nota
    AGTNote *note = [self.fetchedResultsController
                     objectAtIndexPath:indexPath];
    
    // la celda
    static NSString *cellId = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellId];
    }
    // sincronizarlos
    cell.imageView.image = note.photo.image;
    cell.textLabel.text = note.name;
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateStyle = NSDateFormatterMediumStyle;
    cell.detailTextLabel.text = [fmt stringFromDate:note.modificationDate];
    
    // devolver
    return cell;
    
}

#pragma mark - Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar la nota
    AGTNote *note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear un NoteVC
    AGTNoteViewController *noteVC = [[AGTNoteViewController alloc] initWithModel:note];
    
    // Hacer el push
    [self.navigationController pushViewController:noteVC
                                         animated:YES];
    
}
@end














