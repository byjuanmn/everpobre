//
//  AGTPhotoViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 17/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTNote;

@interface AGTPhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (nonatomic, strong) AGTNote *model;

- (IBAction)takePhoto:(id)sender;
- (IBAction)applyFilters:(id)sender;
- (IBAction)deleteImage:(id)sender;

-(id) initWithModel:(AGTNote *) model;


@end







