//
//  AGTAppDelegate.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 11/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTAppDelegate.h"
#import "AGTCoreDataStack.h"
#import "AGTNote.h"
#import "AGTNotebook.h"
#import "AGTNotebooksViewController.h"

#import "UIViewController+Navigation.h"

@implementation AGTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    
    // Crear una instancia del stack (modelo)
    self.model = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
    
    //[self trastearConDatos];
    
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNotebook entityName]];
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.name
                             ascending:YES
                             selector:@selector(caseInsensitiveCompare:)],
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.modificationDate
                             ascending:NO]];
    
    
    NSFetchedResultsController *fetched = [[NSFetchedResultsController alloc]
                                           initWithFetchRequest:req
                                           managedObjectContext:self.model.context
                                           sectionNameKeyPath:nil
                                           cacheName:nil];
    
    // Creo un controlador
    AGTNotebooksViewController *nbVC = [[AGTNotebooksViewController alloc]
                                            initWithFetchedResultsController:fetched
                                            style:UITableViewStylePlain];
    
    
    
    self.window.rootViewController = [nbVC wrappedInNavigation];
    
    // Inicio el autosave
    [self autoSave];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error guardando\n%@", error);
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"La cagamos %@", error);
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"Adiós mundo cruel");
}



-(void) application:(UIApplication *)application
handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler{
    
    
}

-(void) trastearConDatos{
    
    
    // Una libreta
    AGTNotebook *nb = [AGTNotebook notebookWithName:@"Ex-Novias para el recuerdo"
                                          inContext:self.model.context];
    // Una nota
    AGTNote *n = [AGTNote noteWithName:@"Camila Dávalos"
                              notebook:nb
                             inContext:self.model.context];
    
    
    NSLog(@"%@", n.modificationDate);
    n.name = @"Adriana Lima";
    NSLog(@"%@", n.modificationDate);
    
    
    AGTNote *nn = [AGTNote noteWithName:@"Camila Dávalos"
                               notebook:nb
                              inContext:self.model.context];
    
    
    // Buscar
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNote entityName]];
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.name
                             ascending:YES],
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.modificationDate
                             ascending:NO]];
    
    NSError *error = nil;
    NSArray *results = [self.model.context executeFetchRequest:req
                                                         error:&error];
    if (results == nil) {
        // La cagamos
        NSLog(@"La cagamos al buscar, maifrén!. %@", error);
    }else{
        NSLog(@"Notas: %@", results);
    }
    
    
    // Guardar
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"la cagamos al guardar: %@", error);
    }];
}



-(void) autoSave{
    
    if (AUTO_SAVE) {
        NSLog(@"Auto guardando...");
        
        
        // guardo
        [self.model saveWithErrorBlock:^(NSError *error) {
            NSLog(@"la cagaste, Burt Lancaster");
        }];
        
        // llámame pasados 5 segundos
        [self performSelector:@selector(autoSave)
                   withObject:nil
                   afterDelay:AUTO_SAVE_DELAY];

    }
    
}














@end
