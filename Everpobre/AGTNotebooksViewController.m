//
//  AGTNotebooksViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 13/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTNotebooksViewController.h"
#import "AGTNotebook.h"
#import "AGTNote.h"
#import "AGTNotesViewController.h"

@interface AGTNotebooksViewController ()

@end

@implementation AGTNotebooksViewController

-(NSString *)title{
    return @"Everpobre";
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    UIBarButtonItem *add = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNotebook:)];
    
    self.navigationItem.rightBarButtonItem = add;
}

-(void)addNotebook:(id) sender{
    
    AGTNotebook *newNb = [AGTNotebook notebookWithName:@"aaaa Más ex-novias para el recuerdo" inContext:self.fetchedResultsController.managedObjectContext];
    
    NSLog(@"Una nueva libreta: %@", newNb);
}

#pragma mark - data source
-(UITableViewCell *) tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar cual es la libreta
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // pedir una celda o crearla de cero
    static NSString *cellId = @"celda";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        // No había celda para reaprovechar y creamos una de cero
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellId];
    }
    // Sincronizamos modelo y vista (libreta y celda)
    cell.textLabel.text = nb.name;
    
    // devolvemos
    return cell;
}

#pragma mark - delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // la libreta
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // fetchrequest
    NSFetchRequest *req = [NSFetchRequest
                           fetchRequestWithEntityName:
                           [AGTNote entityName]];
    
    req.sortDescriptors = @[[NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.creationDate ascending:NO],
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNamedEntityAttributes.name
                             ascending:YES
                             selector:@selector(caseInsensitiveCompare:)]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"notebook = %@", nb];
    req.predicate = pred;
    
    // fetchedresults
    NSFetchedResultsController *ctrl = [[NSFetchedResultsController alloc]
                                        initWithFetchRequest:req
                                        managedObjectContext:nb.managedObjectContext
                                        sectionNameKeyPath:nil
                                        cacheName:nil];
    
    // controlador
    AGTNotesViewController *notesVC = [[AGTNotesViewController alloc]
                                       initWithNotebook:nb
                                       fetchedResultsController:ctrl
                                       style:UITableViewStylePlain];
    
    // Push que te crió
    [self.navigationController pushViewController:notesVC
                                         animated:YES];
    
}



@end











