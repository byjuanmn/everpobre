//
//  AGTNotesViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 14/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@class AGTNotebook;

@interface AGTNotesViewController : AGTCoreDataTableViewController

-(id) initWithNotebook:(AGTNotebook *) notebook
fetchedResultsController:(NSFetchedResultsController *) controller
                 style:(UITableViewStyle) style;


@end









