//
//  AGTPhotoViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 17/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTPhotoViewController.h"
#import "AGTNote.h"
#import "AGTPhoto.h"

@interface AGTPhotoViewController ()
@end

@implementation AGTPhotoViewController

#pragma mark - init
-(id) initWithModel:(AGTNote *) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
        self.title = model.name;
    }
    
    return self;
}

#pragma mark -  view life cycle
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.photoView.image = self.model.photo.image;
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    

    
    // creamos uno nuevo
    AGTPhoto *newPhoto = [AGTPhoto photoForNote:self.model
                                      inContext:self.model.managedObjectContext];
    
    newPhoto.image = self.photoView.image;
}


#pragma mark -  actions
- (IBAction)takePhoto:(id)sender {
    
    // Intentad hacerlo en casa
}

- (IBAction)applyFilters:(id)sender {
}

- (IBAction)deleteImage:(id)sender {
    
    
    // borro del modelo
    self.model.photo = nil;
    
    // borro de la vista
    CGRect oldBounds = self.photoView.bounds;
    [UIView animateWithDuration:0.8
                          delay:0
                        options:0
                     animations:^{
                         self.photoView.bounds = CGRectZero;
                         self.photoView.alpha = 0;
                         self.photoView.transform = CGAffineTransformMakeRotation(M_2_PI);
                         
                     } completion:^(BOOL finished) {
                         self.photoView.image = nil;
                         self.photoView.bounds = oldBounds;
                         self.photoView.alpha = 1;
                         self.photoView.transform = CGAffineTransformIdentity;
                     }];
    
    
    
    
}
@end













