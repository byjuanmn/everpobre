//
//  AGTNoteViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 17/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "AGTNoteViewController.h"
#import "AGTNote.h"
#import "AGTPhotoViewController.h"

@interface AGTNoteViewController ()
@property (nonatomic, strong) AGTNote *model;
@property (nonatomic) CGRect oldRect;
@end

@implementation AGTNoteViewController




#pragma mark - init
-(id)initWithModel:(AGTNote *) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
    }
    return self;
}

#pragma mark - View Life cycle
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Asignamos delegado
    self.nameView.delegate = self;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // sincronizar vista con modelo
    self.textView.text = self.model.text;
    self.nameView.text = self.model.name;
    self.title = self.model.name;
    
    
    // Alta en notificaciones
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(notifyKeyboardWillAppear:)
               name:UIKeyboardWillShowNotification
             object:nil];
    
    [nc addObserver:self
           selector:@selector(notifyKeyboardWillDisappear:)
               name:UIKeyboardWillHideNotification
             object:nil];
    
    
    // Configuramos una barra de teclado para la textView
    UIBarButtonItem *hideKeyboard = [[UIBarButtonItem alloc]
                                     initWithTitle:@"Hide"
                                     style:UIBarButtonItemStylePlain
                                     target:self
                                     action:@selector(removeKeyboard:)];
    
    UIBarButtonItem *displayTextField = [[UIBarButtonItem alloc]
                                         initWithTitle:@"Name"
                                         style:UIBarButtonItemStylePlain
                                         target:self
                                         action:@selector(displayName:)];
    
    UIToolbar *keyboardToolbar = [[UIToolbar alloc]init];
    keyboardToolbar.items = @[hideKeyboard, displayTextField];
    self.textView.inputAccessoryView = keyboardToolbar;
    keyboardToolbar.barStyle = UIBarStyleDefault;
    keyboardToolbar.translucent = NO;
    
    
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // sincronizar modelo con vista
    self.model.text = self.textView.text;
    self.model.name = self.nameView.text;
    
    
    // baja en todas las notificaciones
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    
    
}

#pragma mark -  Notifications
//UIKeyboardWillShowNotification
-(void)notifyKeyboardWillAppear: (NSNotification *) notification{
    
    // Obtener el frame del teclado
    NSDictionary *info = notification.userInfo;
    NSValue *keyFrameValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyFrame = [keyFrameValue CGRectValue];
    
    
    // La duración de la animación del teclado
    double duration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // Nuevo CGRect
    self.oldRect = self.textView.frame;
    CGRect newRect = CGRectMake(self.oldRect.origin.x,
                                self.oldRect.origin.y,
                                self.oldRect.size.width,
                                self.oldRect.size.height - keyFrame.size.height + self.toolbarView.frame.size.height - 10);
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         self.textView.frame = newRect;
                     } completion:^(BOOL finished) {
                         //
                     }];
    
}

// UIKeyboardWillHideNotification
-(void)notifyKeyboardWillDisappear: (NSNotification *) notification{
    
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         self.textView.frame = self.oldRect;
                     } completion:^(BOOL finished) {
                         //
                     }];
}

#pragma mark -  actions
-(void) displayName:(id) sender{
    
    [self.nameView becomeFirstResponder];
    
}

- (IBAction)displayPhoto:(id)sender {
    
    AGTPhotoViewController *pVC = [[AGTPhotoViewController alloc] initWithModel:self.model];
    
    // hago el push
    [self.navigationController pushViewController:pVC
                                         animated:YES];
    
}

-(IBAction)removeKeyboard:(id)sender{
    
    // Me la pela quien tenga el teclado: que
    // lo suelte ya.
    [self.view endEditing:YES];
    
}

#pragma mark - UITextFieldDelegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    // Comprobar que el texto es correcto (si corresponde)
    
    // ocultar el teclado
    [textField resignFirstResponder];
    
    // devolvemos YES/NO según el contenido del texto
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    // Si llegamos hasta aquí, el texto está bien
    // puedes copiarlo
    NSLog(@"He terminado de editar");
    
}















@end











