//
//  AGTNoteViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 17/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTNote;

@interface AGTNoteViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)displayPhoto:(id)sender;
-(IBAction)removeKeyboard:(id)sender;

-(id)initWithModel:(AGTNote *) model;

@end









