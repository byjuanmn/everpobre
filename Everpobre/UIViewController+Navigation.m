//
//  UIViewController+Navigation.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 13/03/14.
//  Copyright (c) 2014 Agbo. All rights reserved.
//

#import "UIViewController+Navigation.h"

@implementation UIViewController (Navigation)

-(UINavigationController *)wrappedInNavigation{
    
    UINavigationController *navVC = [[UINavigationController alloc] init];
    [navVC pushViewController:self
                     animated:NO];
    return navVC;
}
@end
