#import "AGTNotebook.h"


@interface AGTNotebook ()

+(NSArray *)observableKeys;

@end


@implementation AGTNotebook

+(NSArray *)observableKeys{
    return @[AGTNamedEntityAttributes.name,
             AGTNamedEntityAttributes.creationDate,
             AGTNotebookRelationships.notes];
}

+(instancetype) notebookWithName:(NSString *)name
                       inContext: (NSManagedObjectContext *) context{
    
    AGTNotebook *nb = [AGTNotebook insertInManagedObjectContext:context];
    nb.name = name;
    nb.creationDate = [NSDate date];
    nb.modificationDate = [NSDate date];
    
    return nb;
}




@end






