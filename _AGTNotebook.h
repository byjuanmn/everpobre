// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTNotebook.h instead.

#import <CoreData/CoreData.h>
#import "AGTNamedEntity.h"

extern const struct AGTNotebookAttributes {
} AGTNotebookAttributes;

extern const struct AGTNotebookRelationships {
	__unsafe_unretained NSString *notes;
} AGTNotebookRelationships;

extern const struct AGTNotebookFetchedProperties {
} AGTNotebookFetchedProperties;

@class AGTNote;


@interface AGTNotebookID : NSManagedObjectID {}
@end

@interface _AGTNotebook : AGTNamedEntity {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AGTNotebookID*)objectID;





@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;





@end

@interface _AGTNotebook (CoreDataGeneratedAccessors)

- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(AGTNote*)value_;
- (void)removeNotesObject:(AGTNote*)value_;

@end

@interface _AGTNotebook (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;


@end
