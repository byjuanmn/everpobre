#import "_AGTNote.h"

@interface AGTNote : _AGTNote

+(instancetype) noteWithName:(NSString *) name
                    notebook:(AGTNotebook *) notebook
                   inContext:(NSManagedObjectContext *) context;


@end
