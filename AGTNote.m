#import "AGTNote.h"
#import "AGTPhoto.h"

@interface AGTNote ()

+(NSArray *)observableKeys;
@end


@implementation AGTNote
#pragma mark - Properties
-(void) setPhoto: (AGTPhoto *) newPhoto{
    
    if (self.photo) {
        // Pillamos el moc
        NSManagedObjectContext *moc = self.managedObjectContext;
        
        // matar al AGTPhoto viejo
        [moc deleteObject:self.photo];
    }
    
    // Aviso que voy a cambiar la propiedad (setPrimitiveValue: NO
    // lo hace por nosotros).
    [self willChangeValueForKey:AGTNoteRelationships.photo];
    
    // guardar el nuevo en Core Data
    [self setPrimitiveValue:newPhoto
            forKey:AGTNoteRelationships.photo];
    
    // Ya lo he cambiao, que lo sepasss...
    [self didChangeValueForKey:AGTNoteRelationships.photo];
    
}


+(NSArray *)observableKeys{
    return @[AGTNamedEntityAttributes.name,
             AGTNamedEntityAttributes.creationDate,
             AGTNoteAttributes.text,
             AGTNoteRelationships.photo,
             [NSString stringWithFormat:@"%@.%@", AGTNoteRelationships.photo, AGTPhotoAttributes.photoData]];
}

+(instancetype) noteWithName:(NSString *) name
                    notebook:(AGTNotebook *) notebook
                   inContext:(NSManagedObjectContext *) context{
    
    AGTNote *n = [AGTNote insertInManagedObjectContext:context];
    n.name = name;
    n.notebook = notebook;
    n.creationDate = [NSDate date];
    n.modificationDate = [NSDate date];
    
    return n;
}


@end
