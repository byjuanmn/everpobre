#import "AGTPhoto.h"


@interface AGTPhoto ()

// Private interface goes here.

@end


@implementation AGTPhoto

#pragma mark - Properties
@synthesize image=_image;

-(UIImage *) image{
    
    return [UIImage imageWithData:self.photoData];
}

-(void) setImage:(UIImage *)image{
    
    self.photoData = UIImageJPEGRepresentation(image, 0.9);
}

#pragma mark - Class Methods
+(instancetype) photoForNote:(AGTNote *) note
                   inContext:(NSManagedObjectContext *) context{
    
    AGTPhoto *photo = [AGTPhoto insertInManagedObjectContext:context];
    photo.note = note;
    
    return photo;
    
    
}

@end





