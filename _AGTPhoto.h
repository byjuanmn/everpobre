// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTPhoto.h instead.

#import <CoreData/CoreData.h>


extern const struct AGTPhotoAttributes {
	__unsafe_unretained NSString *photoData;
} AGTPhotoAttributes;

extern const struct AGTPhotoRelationships {
	__unsafe_unretained NSString *note;
} AGTPhotoRelationships;

extern const struct AGTPhotoFetchedProperties {
} AGTPhotoFetchedProperties;

@class AGTNote;



@interface AGTPhotoID : NSManagedObjectID {}
@end

@interface _AGTPhoto : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AGTPhotoID*)objectID;





@property (nonatomic, strong) NSData* photoData;



//- (BOOL)validatePhotoData:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) AGTNote *note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;





@end

@interface _AGTPhoto (CoreDataGeneratedAccessors)

@end

@interface _AGTPhoto (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitivePhotoData;
- (void)setPrimitivePhotoData:(NSData*)value;





- (AGTNote*)primitiveNote;
- (void)setPrimitiveNote:(AGTNote*)value;


@end
