#import "_AGTPhoto.h"

@interface AGTPhoto : _AGTPhoto

@property (strong, nonatomic) UIImage *image;

+(instancetype) photoForNote:(AGTNote *) note
                   inContext:(NSManagedObjectContext *) context;


@end
