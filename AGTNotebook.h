#import "_AGTNotebook.h"

@interface AGTNotebook : _AGTNotebook


+(instancetype) notebookWithName:(NSString *)name
                       inContext: (NSManagedObjectContext *) context;

@end
