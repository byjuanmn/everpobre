#import "AGTNamedEntity.h"


@interface AGTNamedEntity ()

+(NSArray *) observableKeys;
@end


@implementation AGTNamedEntity

+(NSArray *)observableKeys{
    return @[AGTNamedEntityAttributes.name,
             AGTNamedEntityAttributes.creationDate];
}

#pragma mark - Lifecycle
-(void)awakeFromInsert{
    [super awakeFromInsert];
    
    [self setupKVO];
}

-(void)awakeFromFetch{
    [super awakeFromFetch];
    
    [self setupKVO];
}

-(void) willTurnIntoFault{
    [super willTurnIntoFault];
    
    [self tearDownKVO];
    
}



#pragma mark - KVO
-(void) setupKVO{
    
 
    for (NSString *key in [self.class observableKeys]) {
        [self addObserver:self
               forKeyPath:key
                  options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                  context:NULL];
    }
    
}

-(void)tearDownKVO{
    
    for (NSString *key in [self.class observableKeys]) {
        
        [self removeObserver:self
                  forKeyPath:key];
    }
    
}

-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    
    self.modificationDate = [NSDate date];
    
}











@end










